 #!/bin/sh

 # Exit there is not enough parameters.
 if [[ $# -lt 3 ]]
 then
   echo "*---------------*"
   echo "* edrtax.sh 1.0 *"
   echo "*---------------*---------------------------------*"
   echo "* Usage: edrtax.sh OrderNumber DocType Operation  *"
   echo "* i.e. : edrtax.sh 400001234849 SalesOrder create *"
   echo "*-------------------------------------------------*"
   exit 1
 fi

# echo "Checking Parms..."
# echo "OrderNumber=${1}"
# echo "DocType=${2}"
# echo "Operation=${3}"
ulimit -S -n 65534
cd /QOpenSys/edrprod/python/edrtax
. ./edrtaxenv/bin/activate
python EDRTAX.py $@
deactivate
