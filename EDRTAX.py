#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
# =============================================================================
# Created By   : Kenneth Grover <kgrover@e-dr.com>
# Created Date : Thu January 18 2022
# Maintainer   : Kenneth Grover <kgrover@e-dr.com>
# Last Modified: 2022-07-11
# Modified By  : Kenneth Grover <kgrover@e-dr.com>
# =============================================================================
"""
    This program is designed to connect to the AvaTax API to calculate the
    tax on orders.
"""
# =============================================================================
# Imports
# =============================================================================

import os
import cProfile
import io
import pstats
import sys
import time
import json
import traceback
from datetime import date, datetime
from pstats import SortKey

from avalara import AvataxClient

from config import settings
from utils import connect, errors, helper
from utils import process_error as er
from utils import process_response as pr
from utils import tax_check as taxc
from utils import transaction as trn
from utils import base_logger as mylog

# =============================================================================
# Main Program
# =============================================================================

##################################################
###           Performance Profiling            ###
##################################################
# prf = cProfile.Profile()
# prf.enable()

##################################################
###           TESTING ORDER NUMBERS            ###
##################################################

# order_num = '200002761513' # Old order that is not a test
# order_num = '20255282801' # Order that is a state we do not collect tax
# order_num = '200008278425' # Test Order
# order_num = '200090000009' #'400001234695' # Order with shipping
# order_num = '200090000027' # Simulated Walgreens order
# order_num = '200002761513' # Issue with NEWERATEST CompanyCode
# order_num = '200003338720'
# order_num = '200090000094' # New Test Order with multiple destinations
# order_num = '200009152737' # Order For test comparision
# order_num = '400001235097' # Order From Web test account

# doc_type = 'SalesInvoice'
# operation = 'create'
# is_prod = "Y"
# from_program = "local test"
# opseq = '20'




##################################################
###          Main Program Sys Args             ###
##################################################
total_time_start = time.time()
order_num = sys.argv[1]
doc_type = sys.argv[2]
operation = sys.argv[3]
is_prod = sys.argv[4]
from_program = sys.argv[5]
log = mylog.get_newlog(str(order_num))
today = date.today()
dt = today.strftime("%Y-%m-%d")
dt_num = today.strftime("%Y%m%d")
current_time = datetime.now()
tt = current_time.strftime("%H%M%S")
ttdot = current_time.strftime("%H.%M.%S")

test_or_prod = "TEST"
environment = "sandbox"
log.info("New Order: " + str(order_num) + " Entered By: " + from_program)

##################################################
###           Create Connection                ###
##################################################
try:
    conn = connect.get_connection()
    cur = conn.cursor()
except Exception as conn_err:
    log.error("Error creating DB2 Connection: "+conn_err)

opseq = helper.get_opseq(cur, order_num, doc_type, log)

helper.log_activity(order_num, doc_type, today, operation, from_program, "PY BEGIN", "INFO: EDRTAX.py has started.", cur)
# def main(total_time_start, order_num, doc_type, operation, is_prod, from_program, today, dt, dt_num,
#          current_time, tt, opseq, test_or_prod, environment):
#################################################
# Determine whether order is a test or live order
#################################################
start_time = time.time()

if is_prod == "Y":
    test_or_prod = "PROD"
elif is_prod == "N":
    test_or_prod = "TEST"
    
if taxc.is_test(cur, order_num):
    test_or_prod = "TEST"
# else:
#     test_or_prod = 'PROD'
end_time = "--- %s seconds ---" % (time.time() - start_time)
log.debug(
    "Order Number: "
    + str(order_num)
    + " OpSeq: "
    + str(opseq)
    + " is test execute time: "
    + end_time
)


#################################################
# Set enviroment based on test or live order
#################################################

if test_or_prod == "TEST":
    environment = "sandbox"
elif test_or_prod == 'PROD':     ### COMMENT WHEN TESTING
    environment = 'production'   ### COMMENT WHEN TTESTING


#################################################
###   Set AvaTax Client connection details    ###
#################################################
start_time = time.time()
crule = helper.get_crule(cur, order_num, log)
company_details = helper.get_company(crule, test_or_prod)
end_time = "--- %s seconds ---" % (time.time() - start_time)
log.debug(
    "Order Number: "
    + str(order_num)
    + " OpSeq: "
    + str(opseq)
    + " set company rule execute time: "
    + end_time
)

if environment == "sandbox":
    user = settings.sandbox_user
    pwd = settings.sandbox_pass
    
elif environment == "production" and crule == "NEWERA":
    user = settings.NEWERA_production_user
    pwd = settings.NEWERA_production_pass
    
elif environment == "production" and crule != "NEWERA":
    user = settings.EDR_production_user
    pwd = settings.EDR_production_pass




#################################################
###   Check if tax needs to be calculated     ###
#################################################
try:
    start_time = time.time()
    if not taxc.do_tax_check(cur, order_num, crule, company_details, opseq, log):
        helper.log_activity(order_num, doc_type, today, operation, from_program, "PY END", "INFO: Order does not require tax reporting. EDRTAX.py Ending.", cur)
        sys.exit()
    end_time = "--- %s seconds ---" % (time.time() - start_time)
    log.debug(
        "Order Number: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
        + " tax check execute time: "
        + end_time
    )
except Exception as berr:
    stack = traceback.format_exc()
    log.error(berr)
    log.error(stack)
    er.log_sys_error(from_program, "EDRTAXPY", dt_num, tt, berr, cur)


#################################################
###        Set AvaTax Client and Ping         ###
#################################################
try:
    start_time = time.time()
    submit_pgm = "Python SDK " + from_program
    client = AvataxClient(
        company_details["companyCode"], "ver 0.0", submit_pgm, environment
    )
    client = client.add_credentials(user, pwd)
    end_time = "--- %s seconds ---" % (time.time() - start_time)
    log.debug(
        "Order Number: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
        + " AvaTax set client details execute time: "
        + end_time
    )
except Exception as connerror:
    errmsg = "Order Number: " + str(order_num) + " OpSeq: " + str(opseq) + " Error connecting to AvaTax: " + str(connerror)
    log.error(errmsg)
    er.error_email(errmsg, is_prod)
    er.log_sys_error(from_program, "EDRTAXPY", dt_num, tt, errmsg, cur)
    

#################################################
###        Send transaction to AvaTax         ###
#################################################
try:
    responseparam = {
        "$include": "ForceTimeout"
    }  # Added to doc create_or_adjust_transaction param list when testing error handling

    start_time = time.time()
    doc = trn.build_trans(
        cur, order_num, doc_type, operation, test_or_prod, crule, company_details, log
    )
    with open("TandRData/" + order_num +"_"+str(doc_type).upper()+"_OP_"+str(opseq) + "_SENTDATA.txt", "w") as sentJson:
        sentJson.write(json.dumps(doc, indent=4))

    end_time = "--- %s seconds ---" % (time.time() - start_time)
    log.debug(
        "Order Number: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
        + " build transaction doc execute time: "
        + end_time
    )
except Exception as builderror:
    errmsg = "Order Number: " + str(order_num) + " OpSeq: " + str(opseq) + " Error Building Transaction: " + str(builderror)
    stk = traceback.format_exc()
    log.error(errmsg)
    log.error(stk)
    er.log_sys_error(from_program, "EDRTAXPY", dt_num, tt, errmsg, cur)
    

try:
    start_time = time.time()
    log.info(
        "Creating transaction document. DocType: "
        + doc_type
        + " Order: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
    )
    response = client.create_or_adjust_transaction(doc)
    response_json = response.json()
    end_time = "--- %s seconds ---" % (time.time() - start_time)
    log.debug(
        "Order Number: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
        + " Submitting doc and retreiveing response execute time: "
        + end_time
    )

    #################################################
    ###           Process AvaTax Response         ###
    #################################################
    start_time = time.time()
    log.info("processing tax response Order Number: " + order_num + " OpSeq: " + str(opseq))
    pr.tax_process(
        cur,
        response_json,
        opseq,
        order_num,
        operation,
        doc,
        is_prod,
        company_details,
        doc_type,
        log
    )

    end_time = "--- %s seconds ---" % (time.time() - start_time)
    log.debug(
        "Order Number: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
        + " process response execute time: "
        + end_time
    )


except Exception as p_error:
    log.error(
        "Order Number: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
        + " tax response processing error: "
        + str(p_error)
    )
    
    errmsg = "Order Number: "+str(order_num)+" OpSeq: "+str(opseq)+" tax response processing error: "+str(p_error)
    
    er.log_sys_error(from_program, "EDRTAXPY", dt_num, tt, errmsg, cur)

    try:

        er.error_process(
            cur, response_json, order_num, doc_type, operation, opseq, from_program, is_prod,dt, ttdot, log
        )
        taxerror = response_json["error"]["message"]
        errorinsert = (
            '<script type="text/javascript">txError='
            + "'"
            + taxerror
            + "'"
            + ";</script>"
        )
        #if is_prod == "N":      ### Remove if clause when going live
            ### and just print errorinsert
        log.error(
            "Order Number: "
            + str(order_num)
            + " OpSeq: "
            + str(opseq)
            + " Processing an error response from avatax"
        )
        print(errorinsert)
        os._exit(1)
    except Exception as err:
        errortxt = errors.cError(
            "System Error: Please contact support if this error persists."
        )
        stack_trace = traceback.format_exc()
        log.error("Order Number: " + str(order_num))
        log.error(err)
        log.error(stack_trace)

        ertxt = str(errortxt)
        errstr = str(err)
        ststr = str(stack_trace)
        ordstr = str(order_num)
        pgmstr = str(from_program)

        ermsg = (
            errstr
            + "\n"
            + ststr
            + "\n"
            + ordstr
            + "\n"
            + pgmstr
            + "\n"
            + "Opseq: "
            + str(opseq)
            + "\n"
            + doc_type
        )
        er.error_email(ermsg, is_prod)
        errorinsert = (
            '<script type="text/javascript">txError=' + "'" + ertxt + "'" + ";</script>"
        )

        error_log_txt = (
            "Tax Sys Error: OrderNum: "
            + ordstr
            + " OpSeq: "
            + str(opseq)
            + " DocType: "
            + doc_type
            + " Error: "
            + errstr
            + " Stack: "
            + ststr
        )
        er.log_sys_error(from_program, "EDRTAXPY", dt_num, tt, error_log_txt, cur)

        #if is_prod == "N":      ### Remove if clause when going live
        print(errorinsert)  ### and just print errorinsert
        
        os._exit(1)
        


finally:
    
    total_time_end = "--- %s seconds ---" % (time.time() - total_time_start)
    log.info(
        "Order Number: "
        + str(order_num)
        + " OpSeq: "
        + str(opseq)
        + " total execute time: "
        + total_time_end
    )
    helper.log_activity(order_num, doc_type, today, operation, from_program, "PY END", "INFO: EDRTAX.py has ended.", cur)
    cur.close()
    conn.close()
    sys.exit()
        
# if __name__ == "__main__":
#     print(main(total_time_start, order_num, doc_type, operation, is_prod, from_program, today, dt, dt_num,
#          current_time, tt, opseq, test_or_prod, environment))

#####   Code Below is for perfoemance profiling   #####

# prf.disable()
# s = io.StringIO()
# ps = pstats.Stats(prf, stream=s).sort_stats("tottime")
# ps.print_stats()


# with open("Profiles/" + dt + "-testprofile.txt", "a+") as f:
#     f.write(s.getvalue())
