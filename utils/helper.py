
import traceback
from datetime import datetime


def get_crule(cur, order_number, log):
    log.debug("getting crule for ordernum: " + order_number)

    cur.execute(
        r"SELECT crule FROM hmi.pohdr join hmi.icustmst ON phcust=cicode WHERE phiord=?",
        (order_number),
    )
    result = cur.fetchone()
    company = result
    if company != None:
        for r in result:
            company = r
        company = company.strip()
    return company


def get_company(crule, test_or_prod):

    company_details = {"companyCode": "", "companyId": ""}

    if test_or_prod == "PROD":
        if crule == "NEWERA":
            company_details["companyCode"] = "NEWERA"
            company_details["companyId"] = "210742"
        else:  # crule == 'EDR':
            company_details["companyCode"] = "EDR"
            company_details["companyId"] = "201744"
    elif test_or_prod == "TEST":
        if crule == "NEWERA":
            company_details["companyCode"] = "NEWERATEST-2"
            company_details["companyId"] = "2786584"
        else:  # crule == 'EDR':
            company_details["companyCode"] = "EDRTEST"
            company_details["companyId"] = "841706"

    return company_details


def get_opseq(cur, order_num, doc_type, log):
    try:
        cur.execute(
            r"SELECT max(hopseq) FROM HMI.EDRTAXHDR WHERE horder=? AND hdoctype=?",
            (order_num, doc_type),
        )
        sq = cur.fetchone()
        if sq[0] == None:
            opseq = 1
        else:
            opseq = sq[0] + 1
        ps = str(opseq)
        log.info("Current opseq for order: " + order_num + " is " + ps)
        return opseq
    except Exception as err:
        log.error("Error getting opseq for order: " + order_num + "\n" + str(err))
        ps = str(opseq)
        log.error("OPSEQ: " + ps)
        stack_trace = traceback.format_exc()
        log.error(stack_trace)
        opseq = 1
        ps = str(opseq)
        log.error("OPSEQ: " + ps)
        return opseq


def check_opseq(cur, order_num, doc_type):
    cur.execute(
        r"SELECT max(hopseq) FROM HMI.EDRTAXHDR WHERE horder=? AND hdoctype=?",
        (order_num, doc_type),
    )
    sq = cur.fetchone()
    if sq[0] == None:
        opseq = 1
        return opseq
    else:
        opseq = sq[0]
    return opseq


def log_activity(order_num, doc_type, date, operation, from_program, label, info, cur):
    currenttime = datetime.now().strftime("%H.%M.%S")
    ordnum = "000"+str(order_num)
    cur.execute(
        r"INSERT INTO HMI.PYTAXLOG(LLABEL, LDATE, LTIME, LORDER, LDOCTYPE, LOPERATION, LFROMPGM, LINFO)VALUES(?,?,?,?,?,?,?,?)",
        (label, date, currenttime, ordnum, doc_type, operation, from_program, info),
    )
    cur.commit()
