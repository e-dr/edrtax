import os
import smtplib
import sys
from contextlib import redirect_stderr, redirect_stdout
from email.message import EmailMessage


def send_email(send, receive, subject, content):
    sender = send
    receivers = receive
    sub = subject
    con = content

    msg = EmailMessage()
    msg.set_content(con)
    msg["Subject"] = sub
    msg["From"] = sender
    msg["To"] = receivers
    with open("/temp.txt", "w") as f:
        with redirect_stderr(f):
            try:
                smtp_obj = smtplib.SMTP("localhost")
                smtp_obj.set_debuglevel(True)
                smtp_obj.send_message(msg)
                smtp_obj.quit()
                emaillog = "Success"
                with open('emailout.txt', "a+") as f:
                    f.write(emaillog + '\n')
            except smtplib.SMTPException:
                emaillog = "Error: unable to send email"
                with open('emailout.txt', "a+") as f:
                    f.write(emaillog + '\n')
