import logging
from datetime import datetime as dt
from logging.config import fileConfig

# Logger base for import into other modules

def get_newlog(ordernum):
    logger = logging
    now = dt.now()
    date_string = now.strftime("%Y_%m_%d")
    filename = "server_log/" + ordernum +"_"+ date_string + "_DEBUG_LOG.log"
    logging.filename = filename
    fileConfig("config/logging.cfg")
    return logger
