import pyodbc
from config.settings import db_pass, db_port, db_server, db_user


def get_connection():
    connection = pyodbc.connect(
        # Driver='{iSeries Access ODBC Driver}',
        Driver="{IBM i Access ODBC Driver}",
        SYSTEM=db_server,
        PORT=db_port,
        UID=db_user,
        PWD=db_pass,
    )

    return connection
