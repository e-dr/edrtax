"""custom.errors

    custom error exceptions to be used in the edrtax program

"""


class cError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return "TaxPgm Error: {0} ".format(self.message)
        else:
            return "Inadequate error handling casused this message. Inform your developoment team to drink a cup of coffee before coding."
