import json
from datetime import date, datetime

from utils import helper

def build_trans(
    cur, order_num, doc_type, operation, TestOrProd, crule, company_details, log
):

    ordernum = order_num
    doctype = doc_type
    operation = operation
    test_or_prod = TestOrProd
    debugLevel = "Normal"
    
        

    postDate = get_date(ordernum, cur, log)
    customerCode = get_customerCode(ordernum, cur, log)
    salespersonCode = get_repCode(ordernum, cur, log)
    commit = get_commit(doctype)
    log.debug("Order Number: "+order_num+" Commit is "+commit)
    adjReason = get_adjReason(doctype, ordernum, cur, log)
    adjDesc = get_adjDesc(adjReason)

    default_address = {
        "line1": "5575 N Lynch Ave",
        "line2": "",
        "line3": "",
        "city": "Chicago",
        "postalCode": "60630-1417",
        "region": "IL",
        "country": "US",
    }

    companyCode = company_details["companyCode"]
    companyId = company_details["companyId"]

    line_items = []
    number = 0
    linetracker = []
    cur.execute(
        r"SELECT phlnno, phpo, phtrk, phqty, phmerc, phplin, phmanf, phprdc, phadr1, phadr2, phcity, phstate, phzip, phext FROM hmi.pohdr WHERE phiord=?",
        (ordernum),
    )
    result = cur.fetchall()
    for (
        phlnno,
        phpo,
        phtrk,
        phqty,
        phmerc,
        phplin,
        phmanf,
        phprdc,
        phadr1,
        phadr2,
        phcity,
        phstate,
        phzip,
        phext,
    ) in result:

        number = phlnno
        linetracker.append(number)
        referenceCode = phpo
        ref1 = phtrk
        ref2 = ""
        quantity = phqty
        amount = phmerc
        tcode = phplin
        manf = phmanf
        prodCode = phprdc
        desc = phmanf + " " + phprdc.strip()
        line1 = phadr1.strip()
        line2 = phadr2.strip()
        city = phcity.strip()
        region = phstate.strip()
        country = "US"
        postalExt = phext.strip()
        postalCode = phzip

        if len(postalExt) > 4:
            postalExt = "0000"
        else:
            if postalExt != "" and postalExt != "0000":
                postalCode = str(phzip).strip() + "-" + str(phext).strip()

        if line1 == "" and line2 != "":
            line1 = line2
            line2 = ""

        ### Returns have negative qty, AvaTax requires positive ###
        if doctype.upper() == "RETURNINVOICE":
            if quantity < 0:
                quantity = quantity * -1
        ###########################################################

        if tcode == 20 or tcode == 40:
            taxCode = "PH050414"
            itemCode = "PH050414"
        elif tcode == 80:
            taxCode = "PH150102"
            itemCode = "PH150102"

        if (
            tcode == 120
            and manf.upper() == "ED"
            and prodCode.upper() != "FREIGHT UPGRADE"
        ):
            taxCode = "PH050414"
            itemCode = "PH050414"

        if (
            tcode == 120
            and manf.upper() == "ED"
            and prodCode.upper() == "FREIGHT UPGRADE"
        ):
            taxCode = "FR"
            itemCode = "FR"

        if tcode == 120 and manf.upper() == "ED" and prodCode.upper() == "SCANNER01":
            taxCode = "P0000000"
            itemCode = "P0000000"

        if tcode != 20 and tcode != 80 and tcode != 120 and tcode != 40:
            taxCode = "P0000000"
            itemCode = "P0000000"

        # number = number + 1
        line_items.append(
            {
                "number": str(number),
                "ref1": str(ref1),
                "ref2": ref2,
                "quantity": str(quantity),
                "amount": str(amount),
                "taxCode": taxCode,
                "itemCode": itemCode,
                "description": desc,
                "addresses": {
                    "shipFrom": default_address,
                    "shipTo": {
                        "line1": line1,
                        "line2": line2,
                        "city": city,
                        "region": region,
                        "country": country,
                        "postalCode": postalCode,
                    },
                },
            }
        )

    #################################################
    ###
    ### Shipping and Handling
    ###
    #################################################
    number = max(linetracker)
    cur.execute(
        r"SELECT phshpc, phlnno, phtrk, phadr1, phadr2, phcity, phstate, phzip, phext, phcust FROM hmi.pohdr WHERE phiord=?",
        (ordernum),
    )
    result2 = cur.fetchall()
    for (
        phshpc,
        phlnno,
        phtrk,
        phadr1,
        phadr2,
        phcity,
        phstate,
        phzip,
        phext,
        phcust,
    ) in result2:
        amount = phshpc
        if amount != 0:
            custNum = phcust
            desc = "Shipping and Handling"
            taxCode = "FR"
            itemCode = "FR"
            line = phlnno
            trk = phtrk
            quantity = "1"
            number = number + 1

            line1 = phadr1.strip()
            line2 = phadr2.strip()
            city = phcity.strip()
            region = phstate.strip()
            country = "US"
            postalExt = phext.strip()
            postalCode = phzip

            ##################################################
            ### Temp fix for Avalara Louisiana destinations
            ##################################################
            if region.upper() == "LA" and custNum != "20000001":
                taxCode = "FRTMP"
                itemCode = "FRTMP"
            ##################################################
            ##################################################

            #################################################
            # Fix for zip extensions and address line shift
            #################################################
            if len(postalExt) > 4:
                postalExt = "0000"
            else:
                if postalExt != "" and postalExt != "0000":
                    postalCode = str(phzip).strip() + "-" + str(phext).strip()

            if line1 == "" and line2 != "":
                line1 = line2
                line2 = ""
            #################################################
            #################################################

            line_items.append(
                {
                    "number": str(number),
                    "ref1": str(trk),
                    "ref2": "",
                    "quantity": quantity,
                    "amount": str(amount),
                    "taxCode": taxCode,
                    "itemCode": itemCode,
                    "description": desc,
                    "addresses": {
                        "shipFrom": default_address,
                        "shipTo": {
                            "line1": line1,
                            "line2": line2,
                            "city": city,
                            "region": region,
                            "country": country,
                            "postalCode": postalCode,
                        },
                    },
                }
            )

    ###  Walgreens Adjustments  ###
    cur.execute(r"SELECT potadj, poline FROM hmi.posummary WHERE poord#=?", (ordernum))
    result3 = cur.fetchall()
    for potadj, poline in result3:
        amount = potadj
        if amount != 0:
            desc = "Shipping and Handling"
            taxCode = "OH010000"
            itemCode = "OH010000"
            line = poline
            quantity = "1"
            number = number + 1

            line_items.append(
                {
                    "number": str(number),
                    "ref1": str(phtrk),
                    "ref2": "",
                    "quantity": quantity,
                    "amount": str(amount),
                    "taxCode": taxCode,
                    "itemCode": itemCode,
                    "description": desc,
                    "addresses": {
                        "shipFrom": default_address,
                        "shipTo": {
                            "line1": line1,
                            "line2": line2,
                            "city": city,
                            "region": region,
                            "country": country,
                            "postalCode": postalCode,
                        },
                    },
                }
            )
    log.debug(adjReason)

    
    if doctype.upper()=='RETURNINVOICE' or doctype.upper()=='RETURNORDER':
        rtndate = old_date(cur, ordernum, log)
        document = {
        "adjustmentReason": adjReason,
        "adjustmentDescription": adjDesc,
        "createTransactionModel": {
            "type": doctype,
            "code": ordernum,
            "companyCode": companyCode,
            "date": postDate,
            "customerCode": customerCode,
            "referenceCode": referenceCode.strip(),
            "salespersonCode": salespersonCode,
            "purchaseOrderNo": ordernum,
            "addresses": {"shipFrom": default_address},
            "lines": line_items,
            "commit": commit,
            "taxOverride":{
                "type":"taxDate",
                "taxDate":rtndate,
                "reason":adjReason
                },
            },
        }
    else:
        document = {
        "adjustmentReason": adjReason,
        "adjustmentDescription": adjDesc,
        "createTransactionModel": {
            "type": doctype,
            "code": ordernum,
            "companyCode": companyCode,
            "date": postDate,
            "customerCode": customerCode,
            "referenceCode": referenceCode.strip(),
            "salespersonCode": salespersonCode,
            "purchaseOrderNo": ordernum,
            "addresses": {"shipFrom": default_address},
            "lines": line_items,
            "commit": commit,
        },
    }


    # document_json = json.dumps(document, indent=4)
    # log.debug(document_json)
    return document


def old_date(cur, ordernum, log):
    cur.execute(r"SELECT phpo FROM hmi.pohdr where phiord=?",(ordernum))
    result = cur.fetchone()
    oldorder = result[0]
    oldorder.lstrip("0")
    oldDate = get_date(oldorder, cur, log)
    return oldDate




###
# Gets date from order or system if no date is posted
###
def get_date(ordernum, cur, log):
    cur.execute(r"SELECT podate FROM hmi.posumpost WHERE poord#=?", (ordernum))
    result = cur.fetchone()
    if result:
        for r in result:
            postdate = r
            log.info("Order Number: "+ordernum+" Date from POSUMPOST: " + str(postdate))
            postdate = str(postdate)
    elif not result:
        cur.execute(r"SELECT postat FROM hmi.posummary WHERE poord#=?", (ordernum))
        postat = cur.fetchone()
        for i in postat:
            stat = i
        log.debug("Order: "+ordernum+" POSTAT: "+str(stat))
        if stat == ' ' or stat == None:
            postdate = str(date.today())
            log.info("Order Number: "+ordernum+" Date from system: " + str(postdate))
        else:
            cur.execute(r"SELECT podate FROM hmi.posummary WHERE poord#=?", (ordernum))
            result = cur.fetchone()
            if result:
                for r in result:
                    postdate = r
                    log.info("Order Number: "+ordernum+" Date from POSUMMARY: " + str(postdate))
                    postdate = str(postdate)
            else:
                postdate = str(date.today())
                log.info("Order Number: "+ordernum+" Date from system: " + str(postdate))

    try:
        dto = datetime.strptime(postdate, "%Y%m%d")
        datenew = dto.strftime("%Y-%m-%d")
    except:
        dto = postdate
        datenew = dto
    return datenew


###
# Gets the customer number
###
def get_customerCode(ordernum, cur, log):
    cur.execute(r"SELECT pocust FROM hmi.posummary WHERE poord#=?", (ordernum))
    result = cur.fetchone()
    for r in result:
        customerCode = str(r)
        log.info("Order Number: "+ordernum+" Customer Code: "+customerCode)
    return customerCode


###
# Gets the rep code
###
def get_repCode(ordernum, cur, log):
    cur.execute(
        r"SELECT ccode3 FROM hmi.icustmst JOIN hmi.posummary on ccust#=pocust WHERE poord#=?",
        (ordernum),
    )
    result = cur.fetchone()
    for r in result:
        repcode = r
        log.debug("Order Number: "+ordernum+" Rep Code: "+repcode)
    return repcode


###
# Determines whether to commit transaction based on doctype
###
def get_commit(doctype):
    commit = "false"
    docupper = doctype.upper()
    if docupper == "SALESINVOICE" or docupper == "RETURNINVOICE" or docupper == "RETURNCHARGEINVOICE":
        commit = "true"
    return commit


###
# Determines the adjustment reason based on doctype
###
def get_adjReason(doctype, ordernum, cur, log):
    dtype = doctype.upper()
    rectype = "A"
    dctype = "SalesInvoice             "
    cur.execute(r"SELECT hdoctype FROM hmi.edrtaxhdr WHERE horder=? AND hrectype=? AND hdoctype=?",(ordernum, rectype, dctype))
    dup = cur.fetchone()
    log.debug(dup)
    if dtype == "SALESINVOICE":
        if dup == None:
            adjReason = "NotAdjusted"
        else:
            adjReason = "Other"
    elif dtype == "RETURNINVOICE":
        adjReason = "ProductReturned"
    elif dtype == "DOCVOIDED":
        adjReason = "DocVoided"
    else:
        adjReason = "NotAdjusted"
    log.debug("Order Number: "+ordernum+" Adjustment reason: "+adjReason)
    return adjReason


###
# Gets Adjustment Description bassed on AdjustmentReason
###
def get_adjDesc(adjReason):
    if adjReason == "NotAdjusted":
        adjDesc = ""
    elif adjReason =="ProductReturned":
        adjDesc = "Product Returned"
    elif adjReason == "DocVoided":
        adjDesc = "Document Voided"
    elif adjReason == "Other":
        adjDesc = "Create or Adjust Transaction"
    else:
        adjDesc = ""
    return adjDesc