import traceback

from utils import errors, helper


def do_tax_check(cur, order_number, crule, company_details, opseq, log):
    if not val_ordernum(order_number):
        log.info("failed val_ordernum OrderNum: " + order_number)
        return False
    elif not check_cart(cur, order_number):
        log.info("failed check_cart OrderNum: " + order_number)
        return False
    elif not state_is_taxed(cur, order_number, crule, company_details, str(opseq), log):
        log.info("failed state_is_taxed OrderNum: " + order_number)
        return False
    elif not is_journal(cur, order_number, str(opseq), log):
        log.info("failed is_journal OrderNum: " + order_number)
        return False
    else:
        log.info("Passed do_tax_check OrderNum: " + order_number + " OpSeq: " + str(opseq))
        return True


def val_ordernum(order_number):
    if isinstance(order_number, int):
        return True
    else:
        try:
            int(order_number)
            return True
        except:
            errordet = errors.cError("Invalid Order Number: " + order_number)


def check_cart(cur, order_number):
    cur.execute(r"SELECT COUNT(*) FROM hmi.pohdr WHERE phiord=?", (order_number))
    result = cur.fetchall()
    for r in result:
        count = r
    if count == 0:
        return False
    else:
        return True


def state_is_taxed(cur, order_number, crule, company_details, opseq, log):

    company = crule  # company_details["companyCode"]
    if company != "NEWERA" and company != "":
        company = "EDR"
    log.debug(
        "Order Number: "
        + order_number
        + " company is: "
        + company
        + " OpSeq: "
        + str(opseq)
    )
    if company == "EDR" or company == "NEWERA":
        taxed = "AVATAXED"
        cur.execute(
            r"SELECT COUNT(*) FROM hmi.pohdr WHERE phiord=? AND UPPER(phstate) IN(SELECT TRIM(cdcode) FROM hmi.utpcode WHERE cduse=? AND cddesc=?)",
            (order_number, taxed, company),
        )
        result = cur.fetchall()
        for r in result:
            count = r
        if count[0] == 0:
            log.info(
                "Tax is not reported in this state. OrderNum: "
                + order_number
                + " OpSeq: "
                + str(opseq)
            )
            return False
        else:
            log.info(
                "Order Number: "
                + order_number
                + " OpSeq: "
                + str(opseq)
                + " passed state is taxed check"
            )
            return True
    else:
        log.warning("Some Other Issue")
        return False


def is_journal(cur, order_number, opseq, log):
    log.debug("called is_journal OrderNum: " + order_number + " OpSeq: " + str(opseq))

    cur.execute(
        r"SELECT COUNT(*) FROM hmi.pohdr WHERE phiord=? AND (phmanf='JE' OR (phmanf='ED' AND phprdc='PRICE ADJ'))",
        (order_number),
    )
    result = cur.fetchall()
    for r in result:
        count = r
    if count[0] > 0:
        log.debug("This is Journal OrderNum: " + order_number + " OpSeq: " + str(opseq))
        return False
    else:
        return True


def is_test(cur, order_number):
    cur.execute(
        r"SELECT COUNT(*) FROM hmi.pohdr JOIN hmi.icustmst ON phcust=cicode WHERE phiord=? AND cflag='T'",
        (order_number),
    )
    result = cur.fetchall()
    for r in result:
        count = r
    if count[0] > 0:
        return True
