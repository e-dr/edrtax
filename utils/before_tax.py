### Input to tax tables before tax processing
import json

from utils import helper


def before_process(cur, doc, opseq, order_num, operation, company_details, doc_type, log):
    data = doc


    # chkopseq = helper.check_opseq(cur, order_num, doc_type)
    # if opseq <= chkopseq:
    #     opseq = helper.get_opseq(cur, order_num, doc_type)


    hdrlist = []
    hdrkey = []
    ### Tax Header data
    h_opseq = opseq
    h_Operation = operation
    h_AdjReason = data["adjustmentReason"]
    h_AdjDesc = data["adjustmentDescription"]
    h_DocType = data["createTransactionModel"]["type"]
    h_TransCode = data["createTransactionModel"]["code"]
    h_PODate = data["createTransactionModel"]["date"]
    h_RepCode = data["createTransactionModel"]["salespersonCode"]
    h_CustCode = data["createTransactionModel"]["customerCode"]
    h_PONum = data["createTransactionModel"]["purchaseOrderNo"]
    h_RefCode = data["createTransactionModel"]["referenceCode"]
    h_CompanyCode = data["createTransactionModel"]["companyCode"]
    h_CompanyId = company_details["companyId"]
    h_RecType = "B"
    h_OrderNum = order_num
    h_commit = data["createTransactionModel"]["commit"]

    hdrlist.extend(
        [
            h_opseq,
            h_Operation,
            h_AdjReason,
            h_AdjDesc,
            h_DocType,
            h_TransCode,
            h_CompanyCode,
            h_CompanyId,
            h_PODate,
            h_RepCode,
            h_CustCode,
            h_PONum,
            h_RefCode,
            h_RecType,
            h_OrderNum,
            h_commit
        ]
    )

    hdrkey.extend(
        [
            h_OrderNum,
            h_Operation,
            h_opseq,
            h_RecType,
            h_DocType
        ]
    )

    log.info("Order Number: "+order_num+" OpSeq: "+str(opseq)+' List of edrtaxhdr key before value')
    log.info(hdrkey)

    cur.execute(
        r"INSERT INTO HMI.EDRTAXHDR(HOPSEQ, HOPERATION, HCOMMIT, HADJREASON, HADJDESC, HDOCTYPE, HTRANSCODE, HCOMPANYCD, HCOMPANYID, HPODATE, HREPCODE, HCUSTCODE, HPONUM, HREFCODE, HRECTYPE, HORDER)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
        (
            h_opseq,
            h_Operation,
            h_commit,
            h_AdjReason,
            h_AdjDesc,
            h_DocType,
            h_TransCode,
            h_CompanyCode,
            h_CompanyId,
            h_PODate,
            h_RepCode,
            h_CustCode,
            h_PONum,
            h_RefCode,
            h_RecType,
            h_OrderNum,
        ),
    )
    cur.commit()

    ### Line Data
    linelist = []
    for lines in range(len(data["createTransactionModel"]["lines"])):
        l_opseq = opseq
        l_TaxLine = data["createTransactionModel"]["lines"][lines]["number"]
        l_Desc = data["createTransactionModel"]["lines"][lines]["description"]
        l_ItemCode = data["createTransactionModel"]["lines"][lines]["itemCode"]
        l_Amount = data["createTransactionModel"]["lines"][lines]["amount"]
        l_Quantity = data["createTransactionModel"]["lines"][lines]["quantity"]
        l_Ref1 = data["createTransactionModel"]["lines"][lines]["ref1"]
        l_Ref2 = data["createTransactionModel"]["lines"][lines]["ref2"]
        l_OrderNum = order_num
        l_DocType = data["createTransactionModel"]["type"]
        l_RecType = "B"
        l_OrderLine = data["createTransactionModel"]["lines"][lines]["number"]
        l_LinSeq = data["createTransactionModel"]["lines"][lines]["number"]
        l_Operation = operation

        a_fLine1 = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipFrom"
        ]["line1"]
        a_fLine2 = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipFrom"
        ]["line2"]
        a_fLine3 = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipFrom"
        ]["line3"]
        a_fCity = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipFrom"
        ]["city"]
        a_fRegion = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipFrom"
        ]["region"]
        a_fPostCode = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipFrom"
        ]["postalCode"]
        a_fCountry = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipFrom"
        ]["country"]

        a_tLine1 = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipTo"
        ]["line1"]
        a_tLine2 = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipTo"
        ]["line2"]
        # a_tLine3 = data['createTransactionModel']['lines'][lines]['addresses']['shipTo']['line3']
        a_tCity = data["createTransactionModel"]["lines"][lines]["addresses"]["shipTo"][
            "city"
        ]
        a_tRegion = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipTo"
        ]["region"]
        a_tPostCode = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipTo"
        ]["postalCode"]
        a_tCountry = data["createTransactionModel"]["lines"][lines]["addresses"][
            "shipTo"
        ]["country"]

        linelist.extend(
            [
                l_opseq,
                l_TaxLine,
                l_Desc,
                l_ItemCode,
                l_Amount,
                l_Quantity,
                l_Ref1,
                l_Ref2,
                l_OrderNum,
                l_DocType,
                l_RecType,
                l_OrderLine,
                l_LinSeq,
                l_Operation,
                a_fLine1,
                a_fLine2,
                a_fLine3,
                a_fCity,
                a_fRegion,
                a_fPostCode,
                a_fCountry,
                a_tLine1,
                a_tLine2,
                a_tCity,
                a_tRegion,
                a_tPostCode,
                a_tCountry,
            ]
        )


        cur.execute(
            r"INSERT INTO HMI.EDRTAXLIN(LOPSEQ, LTAXLINE, LDESC, LITEMCODE, LAMOUNT, LQUANTITY, LREF1, LREF2, LORDER, LDOCTYPE, LRECTYPE, LORDERLINE, LLINSEQ, LOPERATION, LFRMLINE1, LFRMLINE2, LFRMLINE3, LFRMCITY, LFRMREGN, LFRMPSTCD, LFRMCNTRY, LTOLINE1, LTOLINE2, LTOCITY, LTOREGN, LTOPSTCD, LTOCNTRY)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            (
                l_opseq,
                l_TaxLine,
                l_Desc,
                l_ItemCode,
                l_Amount,
                l_Quantity,
                l_Ref1,
                l_Ref2,
                l_OrderNum,
                l_DocType,
                l_RecType,
                l_OrderLine,
                l_LinSeq,
                l_Operation,
                a_fLine1,
                a_fLine2,
                a_fLine3,
                a_fCity,
                a_fRegion,
                a_fPostCode,
                a_fCountry,
                a_tLine1,
                a_tLine2,
                a_tCity,
                a_tRegion,
                a_tPostCode,
                a_tCountry,
            ),
        )
        cur.commit()
