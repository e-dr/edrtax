import json
import pyodbc
from utils import before_tax, helper


def tax_process(
    cur, response, opseq, order_num, operation, doc, is_prod, company_details, doc_type, log
):

    # Process incoming response data
    data = response

    with open(
        "TandRData/"
        + order_num
        + "_"
        + str(doc_type).upper()
        + "_OP_"
        + str(opseq)
        + "_RESPONSEDATA.txt",
        "w",
    ) as sentJson:
        sentJson.write(json.dumps(data, indent=4))

    before = True
    err_count = 0
    while before:
        try:
            ############################################################################################################
            ##########     Possible opseq duplicate key error solution #################################################
            ############################################################################################################
            log.debug(
                "Checking for duplicate OpSeq, Order Number: "
                + order_num
                + " OpSeq: "
                + str(opseq)
            )
            chkopseq = helper.check_opseq(cur, order_num, doc_type)
            if opseq <= chkopseq:
                log.debug(
                    "Duplicate opseq detected for order "
                    + order_num
                    + " Old opseq: "
                    + str(opseq)
                    + ". Getting New opseq"
                )
                opseq = helper.get_opseq(cur, order_num, doc_type, log)
                log.info("New opseq: " + str(opseq))

            ############################################################################################################
            ############################################################################################################
            ############################################################################################################

            # Add Order info to tax tables as before processing
            log.info(
                "Order Number: "
                + order_num
                + " OpSeq: "
                + str(opseq)
                + " Processing before data"
            )
            before_tax.before_process(
                cur, doc, opseq, order_num, operation, company_details, doc_type, log
            )
            before = False
            break
        except pyodbc.IntegrityError as err:
            log.error(err)
            if err_count < 2 and err.args[0] == "23000":
                log.error("Failed to input before process data: Trying again.")
                err_count += 1
                continue
            else:
                log.error("Before tax could not be processed")
                log.error(err)
                break
        except Exception as err:
            log.error(err)
            break

    hdrkey = []
    hdrlist = []
    ### Tax Header data
    h_opseq = opseq
    h_ID = data["id"]
    h_DocType = data["type"]
    h_TransCode = data["code"]
    h_CompanyId = data["companyId"]
    h_CompanyCode = company_details["companyCode"]
    h_PODate = data["date"]
    h_RepCode = data["salespersonCode"]
    h_CustCode = data["customerCode"]
    h_UseType = data["customerUsageType"]
    h_Discount = data["totalDiscount"]
    h_PONum = data["purchaseOrderNo"]
    h_ExemptNo = data["exemptNo"]
    h_Recon = data["reconciled"]
    h_RefCode = data["referenceCode"]
    h_SLocCode = data["locationCode"]
    h_BatchCode = data["batchCode"]
    h_CurrCode = data["currencyCode"]
    h_XRate = data["exchangeRate"]
    h_XDate = data["exchangeRateEffectiveDate"]

    h_TaxDate = data["taxDate"]
    h_Status = data["status"]
    h_VendCode = data["customerVendorCode"]
    h_Recon = data["reconciled"]
    h_TotAmount = data["totalAmount"]
    h_TotExempt = data["totalExempt"]
    h_TotTax = data["totalTax"]
    h_TotTxable = data["totalTaxable"]
    h_TotTxAv = data["totalTaxCalculated"]
    h_AdjReason = data["adjustmentReason"]
    h_AdjDesc = doc["adjustmentDescription"]
    h_Locked = data["locked"]
    h_Version = data["version"]
    h_ModDate = data["modifiedDate"]
    h_ModDate = h_ModDate[:22]
    h_ModBy = data["modifiedUserId"]
    h_RecType = "A"
    h_OrderNum = order_num
    h_Operation = operation

    hdrlist.extend(
        [
            h_opseq,
            h_ID,
            h_DocType,
            h_TransCode,
            h_CompanyId,
            h_CompanyCode,
            h_PODate,
            h_RepCode,
            h_CustCode,
            h_UseType,
            h_Discount,
            h_PONum,
            h_ExemptNo,
            h_Recon,
            h_RefCode,
            h_SLocCode,
            h_BatchCode,
            h_CurrCode,
            h_XRate,
            h_XDate,
            h_TaxDate,
            h_Status,
            h_VendCode,
            h_TotAmount,
            h_TotExempt,
            h_TotTax,
            h_TotTxable,
            h_TotTxAv,
            h_AdjReason,
            h_AdjDesc,
            h_Locked,
            h_Version,
            h_ModDate,
            h_ModBy,
            h_RecType,
            h_OrderNum,
            h_Operation,
        ]
    )

    hdrkey.extend([h_OrderNum, h_Operation, h_opseq, h_RecType, h_DocType])

    log.info(
        "Order Number: "
        + order_num
        + " OpSeq: "
        + str(opseq)
        + " List of edrtaxhdr key after value"
    )
    log.info(hdrkey)

    cur.execute(
        r"INSERT INTO HMI.EDRTAXHDR(HOPSEQ, HID, HDOCTYPE, HTRANSCODE, HCOMPANYID, HCOMPANYCD, HPODATE, HREPCODE, HCUSTCODE, HUSETYPE, HDISCOUNT, HPONUM, HEXEMPTNO, HRECON, HREFCODE, HLOCCODE, HBATCHCODE, HCURRCODE, HXRATE, HXDATE, HTAXDATE, HSTATUS, HVENDCODE, HTOTAMOUNT, HTOTEXEMPT, HTOTTAX, HTOTTXABLE, HTOTTXAV, HADJREASON, HADJDESC, HLOCKED, HVERSION, HMODDATE, HMODBY, HRECTYPE, HORDER, HOPERATION)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
        (
            h_opseq,
            h_ID,
            h_DocType,
            h_TransCode,
            h_CompanyId,
            h_CompanyCode,
            h_PODate,
            h_RepCode,
            h_CustCode,
            h_UseType,
            h_Discount,
            h_PONum,
            h_ExemptNo,
            h_Recon,
            h_RefCode,
            h_SLocCode,
            h_BatchCode,
            h_CurrCode,
            h_XRate,
            h_XDate,
            h_TaxDate,
            h_Status,
            h_VendCode,
            h_TotAmount,
            h_TotExempt,
            h_TotTax,
            h_TotTxable,
            h_TotTxAv,
            h_AdjReason,
            h_AdjDesc,
            h_Locked,
            h_Version,
            h_ModDate,
            h_ModBy,
            h_RecType,
            h_OrderNum,
            h_Operation,
        ),
    )
    cur.commit()

    ### Line Data

    lineList = []
    for lines in range(len(data["lines"])):
        l_opseq = opseq
        l_Id = data["lines"][lines]["id"]
        l_transactionId = data["lines"][lines]["transactionId"]
        l_TaxLine = data["lines"][lines]["lineNumber"]
        l_UseType = data["lines"][lines]["customerUsageType"]
        l_entityUseCode = data["lines"][lines]["entityUseCode"]
        l_Desc = data["lines"][lines]["description"]
        l_Discount = data["lines"][lines]["discountAmount"]
        l_ExemptAmt = data["lines"][lines]["exemptAmount"]
        l_ExemptCer = data["lines"][lines]["exemptCertId"]
        l_exemptNo = data["lines"][lines]["exemptNo"]
        l_ItemTxble = data["lines"][lines]["isItemTaxable"]
        l_ItemCode = data["lines"][lines]["itemCode"]
        l_Amount = data["lines"][lines]["lineAmount"]
        l_Quantity = data["lines"][lines]["quantity"]
        l_Ref1 = data["lines"][lines]["ref1"]
        l_Ref2 = data["lines"][lines]["ref2"]
        l_RptDate = data["lines"][lines]["reportingDate"]
        l_Tax = data["lines"][lines]["tax"]
        l_TxableAmt = data["lines"][lines]["taxableAmount"]
        l_TaxCalcd = data["lines"][lines]["taxCalculated"]
        l_TaxCode = data["lines"][lines]["taxCode"]
        l_TaxCodeId = data["lines"][lines]["taxCodeId"]
        l_TaxDate = data["lines"][lines]["taxDate"]
        l_TaxInPrce = data["lines"][lines]["taxIncluded"]
        l_OrderNum = order_num
        l_DocType = data["type"]
        l_RecType = "A"
        l_OrderLine = data["lines"][lines]["lineNumber"]
        l_LinSeq = data["lines"][lines]["lineNumber"]
        l_Operation = operation
        # l_ToRegn = data["lines"][lines]["details"][0]["region"]
        if "destinationAddressId" in data["lines"][lines]:
            destAddID = data["lines"][lines]["destinationAddressId"]
            for address in range(len(data["addresses"])):
                if data["addresses"][address]["id"] == destAddID:
                    l_ToLine1 = data["addresses"][address]["line1"]
                    l_ToLine2 = data["addresses"][address]["line2"]
                    l_ToLine3 = data["addresses"][address]["line3"]
                    l_ToCity = data["addresses"][address]["city"]
                    l_ToRegn = data["addresses"][address]["region"]
                    l_ToCntry = data["addresses"][address]["country"]
                    l_ToPstCd = data["addresses"][address]["postalCode"]
                    if "latitude" in data["addresses"][address]:
                        l_ToLat = data["addresses"][address]["latitude"]
                        l_ToLat = l_ToLat[:10]
                        if "." in l_ToLat:
                            sp_lat = l_ToLat.split(".")
                            l_ToLat = sp_lat[0] + "." + sp_lat[1][:6]
                    else:
                        log.debug("default value used for latitude")
                        l_ToLat = ""
                    if "longitude" in data["addresses"][address]:
                        l_ToLong = data["addresses"][address]["longitude"]
                        l_ToLong = l_ToLong[:10]
                        if "." in l_ToLong:
                            sp_long = l_ToLong.split(".")
                            l_ToLong = sp_long[0] + "." + sp_long[1][:6]
                    else:
                        log.debug("default value used for longitude")
                        l_ToLong = ""
        else:
            l_ToLine1 = ""
            l_ToLine2 = ""
            l_ToLine3 = ""
            l_ToCity = ""
            l_ToRegn = ""
            l_ToCntry = ""
            l_ToPstCd = ""
            l_ToLat = ""
            l_ToLong = ""

        if "originAddressId" in data["lines"][lines]:
            originAddID = data["lines"][lines]["originAddressId"]
            for address in range(len(data["addresses"])):
                if data["addresses"][address]["id"] == originAddID:
                    l_FrmLine1 = data["addresses"][address]["line1"]
                    l_FrmLine2 = data["addresses"][address]["line2"]
                    l_FrmLine3 = data["addresses"][address]["line3"]
                    l_FrmCity = data["addresses"][address]["city"]
                    l_FrmRegn = data["addresses"][address]["region"]
                    l_FrmCntry = data["addresses"][address]["country"]
                    l_FrmPstCd = data["addresses"][address]["postalCode"]
                    if "latitude" in data["addresses"][address]:
                        l_FrmLat = data["addresses"][address]["latitude"]
                        l_FrmLat = l_FrmLat[:10]
                        if "." in l_FrmLat:
                            sp_lat = l_FrmLat.split(".")
                            l_FrmLat = sp_lat[0] + "." + sp_lat[1][:6]
                    else:
                        log.debug("default value used for latitude")
                        l_FrmLat = ""
                    if "longitude" in data["addresses"][address]:
                        l_FrmLong = data["addresses"][address]["longitude"]
                        l_FrmLong = l_FrmLong[:10]
                        if "." in l_FrmLong:
                            sp_long = l_FrmLong.split(".")
                            l_FrmLong = sp_long[0] + "." + sp_long[1][:6]
                    else:
                        log.debug("default value used for longitude")
                        l_FrmLong = ""

        else:
            l_FrmLine1 = ""
            l_FrmLine2 = ""
            l_FrmLine3 = ""
            l_FrmCity = ""
            l_FrmRegn = ""
            l_FrmCntry = ""
            l_FrmPstCd = ""
            l_FrmLat = ""
            l_FrmLong = ""

        lineList.extend(
            [
                l_opseq,
                l_Id,
                l_transactionId,
                l_TaxLine,
                l_UseType,
                l_entityUseCode,
                l_Desc,
                l_Discount,
                l_ExemptAmt,
                l_ExemptCer,
                l_exemptNo,
                l_ItemTxble,
                l_ItemCode,
                l_Amount,
                l_Quantity,
                l_Ref1,
                l_Ref2,
                l_RptDate,
                l_Tax,
                l_TxableAmt,
                l_TaxCalcd,
                l_TaxCode,
                l_TaxCodeId,
                l_TaxDate,
                l_TaxInPrce,
                l_OrderNum,
                l_DocType,
                l_RecType,
                l_OrderLine,
                l_LinSeq,
                l_Operation,
                l_ToLine1,
                l_ToLine2,
                l_ToLine3,
                l_ToCity,
                l_ToRegn,
                l_ToCntry,
                l_ToPstCd,
                l_ToLat,
                l_ToLong,
                l_FrmLine1,
                l_FrmLine2,
                l_FrmLine3,
                l_FrmCity,
                l_FrmRegn,
                l_FrmCntry,
                l_FrmPstCd,
                l_FrmLat,
                l_FrmLong,
            ]
        )
        cur.execute(
            r"INSERT INTO HMI.EDRTAXLIN(LOPSEQ, LID, LTAXLINE, LUSETYPE, LDESC, LDISCOUNT, LEXEMPTAMT, LEXEMPTCER, LITEMTXBLE, LITEMCODE, LAMOUNT, LQUANTITY, LREF1, LREF2, LRPTDATE, LTAX, LTXABLEAMT, LTAXCALCD, LTAXCODE, LTAXCODEID, LTAXDATE, LTAXINPRCE, LORDER, LDOCTYPE, LRECTYPE, LORDERLINE, LLINSEQ, LOPERATION, LTOLINE1, LTOLINE2, LTOLINE3, LTOCITY, LTOREGN, LTOCNTRY, LTOPSTCD, LTOLAT, LTOLONG, LFRMLINE1, LFRMLINE2, LFRMLINE3, LFRMCITY, LFRMREGN, LFRMCNTRY, LFRMPSTCD, LFRMLAT, LFRMLONG)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            (
                l_opseq,
                l_Id,
                l_TaxLine,
                l_UseType,
                l_Desc,
                l_Discount,
                l_ExemptAmt,
                l_ExemptCer,
                l_ItemTxble,
                l_ItemCode,
                l_Amount,
                l_Quantity,
                l_Ref1,
                l_Ref2,
                l_RptDate,
                l_Tax,
                l_TxableAmt,
                l_TaxCalcd,
                l_TaxCode,
                l_TaxCodeId,
                l_TaxDate,
                l_TaxInPrce,
                l_OrderNum,
                l_DocType,
                l_RecType,
                l_OrderLine,
                l_LinSeq,
                l_Operation,
                l_ToLine1,
                l_ToLine2,
                l_ToLine3,
                l_ToCity,
                l_ToRegn,
                l_ToCntry,
                l_ToPstCd,
                l_ToLat,
                l_ToLong,
                l_FrmLine1,
                l_FrmLine2,
                l_FrmLine3,
                l_FrmCity,
                l_FrmRegn,
                l_FrmCntry,
                l_FrmPstCd,
                l_FrmLat,
                l_FrmLong,
            ),
        )
        cur.commit()

        detailsList = []
        for details in range(len(data["lines"][lines]["details"])):
            d_id = data["lines"][lines]["details"][details]["id"]
            d_transactionId = data["lines"][lines]["details"][details]["transactionId"]
            d_country = data["lines"][lines]["details"][details]["country"]
            d_region = data["lines"][lines]["details"][details]["region"]
            d_exemptAmt = data["lines"][lines]["details"][details]["exemptAmount"]
            d_jurisCode = data["lines"][lines]["details"][details]["jurisCode"]
            d_jurisName = data["lines"][lines]["details"][details]["jurisName"]
            d_jurisName = d_jurisName[:49]
            d_statAssNo = data["lines"][lines]["details"][details]["stateAssignedNo"]
            d_jurisType = data["lines"][lines]["details"][details]["jurisType"]
            d_nonTaxAmt = data["lines"][lines]["details"][details]["nonTaxableAmount"]
            d_rate = data["lines"][lines]["details"][details]["rate"]
            d_tax = data["lines"][lines]["details"][details]["tax"]
            d_taxAblAmt = data["lines"][lines]["details"][details]["taxableAmount"]
            d_taxType = data["lines"][lines]["details"][details]["taxType"]
            d_taxName = data["lines"][lines]["details"][details]["taxName"]
            d_authTypId = data["lines"][lines]["details"][details]["taxAuthorityTypeId"]
            d_taxCalcd = data["lines"][lines]["details"][details]["taxCalculated"]
            d_rateType = data["lines"][lines]["details"][details]["rateType"]
            d_order = order_num
            d_docType = data["type"]
            d_operation = operation
            d_opseq = opseq
            d_orderLine = data["lines"][lines]["lineNumber"]
            d_dtlseq = details + 1

            detailsList.extend(
                [
                    d_id,
                    d_transactionId,
                    d_country,
                    d_region,
                    d_exemptAmt,
                    d_jurisCode,
                    d_jurisName,
                    d_statAssNo,
                    d_jurisType,
                    d_nonTaxAmt,
                    d_rate,
                    d_tax,
                    d_taxAblAmt,
                    d_taxType,
                    d_taxName,
                    d_authTypId,
                    d_taxCalcd,
                    d_rateType,
                    d_order,
                    d_docType,
                    d_operation,
                    d_opseq,
                    d_orderLine,
                    d_dtlseq,
                ]
            )

            # log.info('Tax Line Details List: ')
            # log.info(detailsList)

            cur.execute(
                r"INSERT INTO HMI.EDRTAXDET(DID, DTRANSID, DCOUNTRY, DREGION, DEXEMPTAMT, DJURISCODE, DJURISNAME, DSTATASSNO, DJURISTYPE, DNONTAXAMT, DRATE, DTAX, DTAXABLAMT, DTAXTYPE, DTAXNAME, DAUTHTYPID, DTAXCALCD, DRATETYPE, DORDER, DDOCTYPE, DOPERATION, DOPSEQ, DORDERLINE, DDTLSEQ)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                (
                    d_id,
                    d_transactionId,
                    d_country,
                    d_region,
                    d_exemptAmt,
                    d_jurisCode,
                    d_jurisName,
                    d_statAssNo,
                    d_jurisType,
                    d_nonTaxAmt,
                    d_rate,
                    d_tax,
                    d_taxAblAmt,
                    d_taxType,
                    d_taxName,
                    d_authTypId,
                    d_taxCalcd,
                    d_rateType,
                    d_order,
                    d_docType,
                    d_operation,
                    d_opseq,
                    d_orderLine,
                    d_dtlseq,
                ),
            )
            cur.commit()

        ###############################################################################
        ###  Update POHDR. For live use only. Comment out when testing
        ###############################################################################

        cur.execute(r"SELECT COUNT (*) FROM HMI.POSUMPOST WHERE POORD#=?", (l_OrderNum))
        posted = cur.fetchone()[0]
        if posted != 0:
            log.debug("Posted")
        else:
            shipping_line = "1"
            # log.info("NOT POSTED")
            if l_Desc.upper() == "SHIPPING AND HANDLING":
                cur.execute(
                    r"UPDATE HMI.POHDR SET phtaxs=phtaxs+? WHERE phiord=? AND phlnno=?",
                    (l_Tax, l_OrderNum, shipping_line),
                )
                cur.commit()
            else:
                cur.execute(
                    r"SELECT phtaxs, phmerc FROM HMI.POHDR WHERE phiord=? AND phlnno=?",
                    (l_OrderNum, l_OrderLine),
                )
                results = cur.fetchall()
                if len(results) == 0:
                    continue
                for phtaxs, phmerc in results:
                    taxs = float(phtaxs)
                    merc = phmerc
                if taxs != 0 and l_Tax == 0 and str(l_Amount) == str(merc):
                    log.info("Don't Zero Tax")
                else:
                    log.debug("Updating POHDR.PHTAXS")
                    cur.execute(
                        r"UPDATE HMI.POHDR SET phtaxs=? WHERE phiord=? AND phlnno=?",
                        (l_Tax, l_OrderNum, l_OrderLine),
                    )
                    cur.commit()

    ### Address data
    addlist = []
    for addr in range(len(data["addresses"])):
        a_AddSeq = addr + 1
        a_LinSeq = a_AddSeq
        a_opseq = opseq
        a_Id = data["addresses"][addr]["id"]
        a_TransId = data["addresses"][addr]["transactionId"]
        a_BoundLev = data["addresses"][addr]["boundaryLevel"]
        a_Line1 = data["addresses"][addr]["line1"]
        a_Line2 = data["addresses"][addr]["line2"]
        a_Line3 = data["addresses"][addr]["line3"]
        a_City = data["addresses"][addr]["city"]
        a_Region = data["addresses"][addr]["region"]
        a_PostCode = data["addresses"][addr]["postalCode"]
        a_Country = data["addresses"][addr]["country"]
        if "latitude" in data["addresses"][addr]:
            a_Lat = data["addresses"][addr]["latitude"]
            a_Lat = a_Lat[:10]
            if "." in a_Lat:
                sp_lat = a_Lat.split(".")
                a_Lat = sp_lat[0] + "." + sp_lat[1][:6]
        else:
            log.debug("default value used for latitude")
            a_Lat = ""
        if "longitude" in data["addresses"][addr]:
            a_Long = data["addresses"][addr]["longitude"]
            a_Long = a_Long[:10]
            if "." in a_Long:
                sp_long = a_Long.split(".")
                a_Long = sp_long[0] + "." + sp_long[1][:6]
        else:
            log.debug("default value used for longitude")
            a_Long = ""
        a_DocType = data["type"]
        a_OrderLine = a_AddSeq
        a_OrderNum = order_num
        a_Operation = operation

        addlist.extend(
            [
                a_opseq,
                a_Id,
                a_TransId,
                a_BoundLev,
                a_Line1,
                a_Line2,
                a_Line3,
                a_City,
                a_Region,
                a_PostCode,
                a_Country,
                a_Lat,
                a_Long,
                a_DocType,
                a_AddSeq,
                a_LinSeq,
                a_OrderLine,
                a_OrderNum,
                a_Operation,
            ]
        )

        cur.execute(
            r"INSERT INTO HMI.EDRTAXADD(AOPSEQ, AID, ATRANSID, ABOUNDLEV, ALINE1, ALINE2, ALINE3, ACITY, AREGION, APOSTCODE, ACOUNTRY, ALAT, ALONG, ADOCTYPE, AADDSEQ, ALINSEQ, AORDERLINE, AORDER, AOPERATION)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            (
                a_opseq,
                a_Id,
                a_TransId,
                a_BoundLev,
                a_Line1,
                a_Line2,
                a_Line3,
                a_City,
                a_Region,
                a_PostCode,
                a_Country,
                a_Lat,
                a_Long,
                a_DocType,
                a_AddSeq,
                a_LinSeq,
                a_OrderLine,
                a_OrderNum,
                a_Operation,
            ),
        )
        cur.commit()

    ### Summary data
    sumlist = []
    for summ in range(len(data["summary"])):
        s_opseq = opseq
        s_SumSeq = summ + 1
        s_DocType = data["type"]
        s_Country = data["summary"][summ]["country"]
        s_Region = data["summary"][summ]["region"]
        s_JurisType = data["summary"][summ]["jurisType"]
        s_JurisCode = data["summary"][summ]["jurisCode"]
        s_JurisName = data["summary"][summ]["jurisName"]
        s_JurisName = s_JurisName[:50]
        s_AuthType = data["summary"][summ]["taxAuthorityType"]
        s_StatAssNo = data["summary"][summ]["stateAssignedNo"]
        s_TaxType = data["summary"][summ]["taxType"]
        s_TaxName = data["summary"][summ]["taxName"]
        s_RateType = data["summary"][summ]["rateType"]
        s_Taxable = data["summary"][summ]["taxable"]
        s_Rate = data["summary"][summ]["rate"]
        s_Tax = data["summary"][summ]["tax"]
        s_TaxCalcd = data["summary"][summ]["taxCalculated"]
        s_NonTaxabl = data["summary"][summ]["nonTaxable"]
        s_Exempt = data["summary"][summ]["exemption"]
        s_OrderNum = order_num
        s_Operation = operation

        sumlist.extend(
            [
                s_opseq,
                s_Country,
                s_Region,
                s_JurisType,
                s_JurisCode,
                s_JurisName,
                s_AuthType,
                s_StatAssNo,
                s_TaxType,
                s_TaxName,
                s_RateType,
                s_Taxable,
                s_Rate,
                s_Tax,
                s_TaxCalcd,
                s_NonTaxabl,
                s_Exempt,
                s_OrderNum,
                s_Operation,
                s_SumSeq,
                s_DocType,
            ]
        )


        cur.execute(
            r"INSERT INTO HMI.EDRTAXSUM(SOPSEQ, SCOUNTRY, SREGION, SJURISTYPE, SJURISCODE, SJURISNAME, SAUTHTYPE, SSTATASSNO, STAXTYPE, STAXNAME, SRATETYPE, STAXABLE, SRATE, STAX, STAXCALCD, SNONTAXABL, SEXEMPT, SORDER, SOPERATION, SSUMSEQ, SDOCTYPE)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            (
                s_opseq,
                s_Country,
                s_Region,
                s_JurisType,
                s_JurisCode,
                s_JurisName,
                s_AuthType,
                s_StatAssNo,
                s_TaxType,
                s_TaxName,
                s_RateType,
                s_Taxable,
                s_Rate,
                s_Tax,
                s_TaxCalcd,
                s_NonTaxabl,
                s_Exempt,
                s_OrderNum,
                s_Operation,
                s_SumSeq,
                s_DocType,
            ),
        )
        cur.commit()
