import json
import os
import sys
from contextlib import redirect_stderr

from utils import helper
from utils.pymail import send_email


def error_process(
    cur,
    response_json,
    order_num,
    doc_type,
    operation,
    opseq,
    from_program,
    is_prod,
    dt,
    ttdot,
    log,
):
    errormsgs = response_json

    opseq = opseq
    operation = operation
    doctype = doc_type
    ordernum = order_num
    if "code" in errormsgs["error"]:
        code = errormsgs["error"]["code"]
    else:
        code = "No Code Given"
    message = errormsgs["error"]["message"]
    if "target" in errormsgs["error"]:
        target = errormsgs["error"]["target"]
    else:
        target = "No given target"

    errhdr = []

    errhdr.extend([operation, doctype, ordernum, code, message, target])
    log.error(errhdr)

    chkopseq = helper.check_opseq(cur, order_num, doc_type)
    if opseq <= chkopseq:
        opseq = helper.get_opseq(cur, order_num, doc_type, log)

    detailslist = []
    for det in range(len(errormsgs["error"]["details"])):
        d_msgseq = det + 1
        d_code = errormsgs["error"]["details"][det]["code"]
        if "number" in errormsgs["error"]["details"][det]:
            d_number = errormsgs["error"]["details"][det]["number"]
        else:
            d_number = "No error number given"
        d_message = errormsgs["error"]["details"][det]["message"]

        d_description = errormsgs["error"]["details"][det]["description"]

        d_faultCode = errormsgs["error"]["details"][det]["faultCode"]
        d_helpLink = errormsgs["error"]["details"][det]["helpLink"]
        if "severity" in errormsgs["error"]["details"][det]:
            d_severity = errormsgs["error"]["details"][det]["severity"]
        else:
            d_severity = "No Severity Giver"

        detailslist.extend(
            [
                d_code,
                d_number,
                d_message,
                d_description,
                d_faultCode,
                d_helpLink,
                d_severity,
                d_msgseq,
                errhdr,
                opseq,
            ]
        )
        # log.debug(detailslist)

        emsg = json.dumps(errormsgs, indent=4)
        error_email(
            str(emsg) + " Order: " + str(ordernum) + " From Program: " + from_program,
            is_prod,
        )

        d_message_trim = d_message[:64]
        d_description_trim = d_description[:128]

        cur.execute(
            r"INSERT INTO HMI.EDRTAXMSG(MCODE, MMESSAGE, MDESC, MREFERSTO, MSEVERITY, MORDER, MDOCTYPE, MOPERATION, MMSGSEQ, MOPSEQ)VALUES(?,?,?,?,?,?,?,?,?,?)",
            (
                d_code,
                d_message_trim,
                d_description_trim,
                d_faultCode,
                d_severity,
                ordernum,
                doctype,
                operation,
                d_msgseq,
                opseq,
            ),
        )
        
        update_tax_control(cur, operation, d_code, d_severity, from_program, d_message_trim, dt, ttdot, order_num, doc_type, opseq)


def update_tax_control(
    cur,
    operation,
    error_code,
    error_severity,
    from_program,
    error_text,
    received_date,
    received_time,
    order_num,
    doc_type,
    opseq
):
    cur.execute(
        r"SELECT max(cctlseq) FROM HMI.EDRTAXCTL WHERE corder=? AND cdoctype=?",
        (order_num, doc_type),
    )
    sq = cur.fetchone()
    if sq[0] == None:
        ctlseq = 1
    else:
        ctlseq = sq[0] + 1

    cur.execute(
        r"INSERT INTO HMI.EDRTAXCTL(COPERATION, CERRORCODE, CERRORSEV, CERRORPGM, CERRORTEXT, CRCVDT, CRCVTM, CORDER, CDOCTYPE, COPSEQ, CCTLSEQ)VALUES(?,?,?,?,?,?,?,?,?,?,?)",
        operation,
        error_code,
        error_severity,
        from_program,
        error_text,
        received_date,
        received_time,
        order_num,
        doc_type,
        opseq,
        ctlseq,
    )
    cur.commit()


def error_email(message, is_prod):
    if is_prod == "Y":
        send = "TAX_ERROR@e-dr.com"
    else:
        send = "DEV_TEST_ERROR@e-dr.com"
    receive = ["kgrover@e-dr.com, Programming@e-dr.com"]
    subject = "EDRTAXAPI:PYTHON TAX ERROR"
    content = message

    with open("/temp.txt", "w") as f:
        with redirect_stderr(f):
            send_email(send, receive, subject, content)


def log_sys_error(pgm, library, dt, tm, message, cur):
    frm_pgm = pgm
    lbry = library
    er_date = dt
    er_time = tm
    er_msg = message[:990]

    cur.execute(
        r"INSERT INTO hmi.errorlogp0(ERRPGM, ERRLIB, ERRDATE, ERRTIME, ERRMSG)VALUES(?,?,?,?,?)",
        (frm_pgm, lbry, er_date, er_time, er_msg),
    )
    cur.commit()
